﻿using System;

namespace First
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("So is this just a normal console application?");
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Clear();
                Console.WriteLine("yes");
                Console.ReadLine();
            }
            Console.ReadLine();
        }
    }
}